<?php
namespace Drupal\update_audit;

class Installer
{
    private $quiet;
    private $disableTls;
    private $cafile;
    private $installPath;
    private $target;
    private $tmpFile;
    private $baseUrl;
    private $algo;
    private $errHandler;
    private $httpClient;
    private $pubKeys = array();
    private $installs = array();

    /**
     * Constructor - must not do anything that throws an exception
     *
     * @param bool $quiet Quiet mode
     * @param bool $disableTls Bypass tls
     * @param mixed $cafile Path to CA bundle, or false
     */
    public function __construct($quiet, $disableTls, $caFile)
    {
        if (($this->quiet = $quiet)) {
            ob_start();
        }
        $this->disableTls = $disableTls;
        $this->cafile = $caFile;
        $this->errHandler = new ErrorHandler();
    }

    /**
     * Runs the installer
     *
     * @param mixed $version Specific version to install, or false
     * @param mixed $installDir Specific installation directory, or false
     * @param string $filename Specific filename to save to, or composer.phar
     * @param string $channel Specific version channel to use
     * @throws Exception If anything other than a RuntimeException is caught
     *
     * @return bool If the installation succeeded
     */
    public function run($version, $installDir, $filename, $channel)
    {
        try {
            $this->initTargets($installDir, $filename);
            $this->initTls();
            $this->httpClient = new HttpClient($this->disableTls, $this->cafile);
            $result = $this->install($version, $channel);
        } catch (Exception $e) {
            $result = false;
        }

        // Always clean up
        $this->cleanUp($result);

        if (isset($e)) {
            // Rethrow anything that is not a RuntimeException
            if (!$e instanceof RuntimeException) {
                throw $e;
            }
            out($e->getMessage(), 'error');
        }
        return $result;
    }

    /**
     * Initialization methods to set the required filenames and composer url
     *
     * @param mixed $installDir Specific installation directory, or false
     * @param string $filename Specific filename to save to, or composer.phar
     * @throws RuntimeException If the installation directory is not writable
     */
    protected function initTargets($installDir, $filename)
    {
        $this->installPath = (is_dir($installDir) ? rtrim($installDir, '/').'/' : '').$filename;
        $installDir = realpath($installDir) ? realpath($installDir) : getcwd();

        if (!is_writeable($installDir)) {
            throw new RuntimeException('The installation directory "'.$installDir.'" is not writable');
        }

        $this->target = $installDir.DIRECTORY_SEPARATOR.$filename;
        $this->tmpFile = $installDir.DIRECTORY_SEPARATOR.basename($this->target, '.phar').'-temp.phar';

        $uriScheme = $this->disableTls ? 'http' : 'https';
        $this->baseUrl = $uriScheme.'://getcomposer.org';
    }

    /**
     * A wrapper around methods to check tls and write public keys
     * @throws RuntimeException If SHA384 is not supported
     */
    protected function initTls()
    {
        if ($this->disableTls) {
            return;
        }

        if (!in_array('SHA384', openssl_get_md_methods())) {
            throw new RuntimeException('SHA384 is not supported by your openssl extension');
        }

        $this->algo = defined('OPENSSL_ALGO_SHA384') ? OPENSSL_ALGO_SHA384 : 'SHA384';
        $home = $this->getComposerHome();

        $this->pubKeys = array(
            'dev' => $this->installKey(self::getPKDev(), $home, 'keys.dev.pub'),
            'tags' => $this->installKey(self::getPKTags(), $home, 'keys.tags.pub')
        );

        if (empty($this->cafile) && !HttpClient::getSystemCaRootBundlePath()) {
            $this->cafile = $this->installKey(HttpClient::getPackagedCaFile(), $home, 'cacert.pem');
        }
    }

    /**
     * Returns the Composer home directory, creating it if required
     * @throws RuntimeException If the directory cannot be created
     *
     * @return string
     */
    protected function getComposerHome()
    {
        $home = getHomeDir();

        if (!is_dir($home)) {
            $this->errHandler->start();

            if (!mkdir($home, 0777, true)) {
                throw new RuntimeException(sprintf(
                    'Unable to create Composer home directory "%s": %s',
                    $home,
                    $this->errHandler->message
                ));
            }
            $this->installs[] = $home;
            $this->errHandler->stop();
        }
        return $home;
    }

    /**
     * Writes public key data to disc
     *
     * @param string $data The public key(s) in pem format
     * @param string $path The directory to write to
     * @param string $filename The name of the file
     * @throws RuntimeException If the file cannot be written
     *
     * @return string The path to the saved data
     */
    protected function installKey($data, $path, $filename)
    {
        $this->errHandler->start();

        $target = $path.DIRECTORY_SEPARATOR.$filename;
        $installed = file_exists($target);
        $write = file_put_contents($target, $data, LOCK_EX);
        @chmod($target, 0644);

        $this->errHandler->stop();

        if (!$write) {
            throw new RuntimeException(sprintf('Unable to write %s to: %s', $filename, $path));
        }

        if (!$installed) {
            $this->installs[] = $target;
        }

        return $target;
    }

    /**
     * The main install function
     *
     * @param mixed $version Specific version to install, or false
     * @param string $channel Version channel to use
     *
     * @return bool If the installation succeeded
     */
    protected function install($version, $channel)
    {
        $retries = 3;
        $result = false;
        $infoMsg = 'Downloading...';
        $infoType = 'info';

        while ($retries--) {
            if (!$this->quiet) {
                out($infoMsg, $infoType);
                $infoMsg = 'Retrying...';
                $infoType = 'error';
            }

            if (!$this->getVersion($channel, $version, $url, $error)) {
                out($error, 'error');
                continue;
            }

            if (!$this->downloadToTmp($url, $signature, $error)) {
                out($error, 'error');
                continue;
            }

            if (!$this->verifyAndSave($version, $signature, $error)) {
                out($error, 'error');
                continue;
            }

            $result = true;
            break;
        }

        if (!$this->quiet) {
            if ($result) {
                out(PHP_EOL."Composer (version {$version}) successfully installed to: {$this->target}", 'success');
                out("Use it: php {$this->installPath}", 'info');
                out('');
            } else {
                out('The download failed repeatedly, aborting.', 'error');
            }
        }
        return $result;
    }

    /**
     * Sets the version url, downloading version data if required
     *
     * @param string $channel Version channel to use
     * @param false|string $version Version to install, or set by method
     * @param null|string $url The versioned url, set by method
     * @param null|string $error Set by method on failure
     *
     * @return bool If the operation succeeded
     */
    protected function getVersion($channel, &$version, &$url, &$error)
    {
        $error = '';

        if ($version) {
            if (empty($url)) {
                $url = $this->baseUrl."/download/{$version}/composer.phar";
            }
            return true;
        }

        $this->errHandler->start();

        if ($this->downloadVersionData($data, $error)) {
            $this->parseVersionData($data, $channel, $version, $url);
        }

        $this->errHandler->stop();
        return empty($error);
    }

    /**
     * Downloads and json-decodes version data
     *
     * @param null|array $data Downloaded version data, set by method
     * @param null|string $error Set by method on failure
     *
     * @return bool If the operation succeeded
     */
    protected function downloadVersionData(&$data, &$error)
    {
        $url = $this->baseUrl.'/versions';
        $errFmt = 'The "%s" file could not be %s: %s';

        if (!$json = $this->httpClient->get($url)) {
            $error = sprintf($errFmt, $url, 'downloaded', $this->errHandler->message);
            return false;
        }

        if (!$data = json_decode($json, true)) {
            $error = sprintf($errFmt, $url, 'json-decoded', $this->getJsonError());
            return false;
        }
        return true;
    }

    /**
     * A wrapper around the methods needed to download and save the phar
     *
     * @param string $url The versioned download url
     * @param null|string $signature Set by method on successful download
     * @param null|string $error Set by method on failure
     *
     * @return bool If the operation succeeded
     */
    protected function downloadToTmp($url, &$signature, &$error)
    {
        $error = '';
        $errFmt = 'The "%s" file could not be downloaded: %s';
        $sigUrl = $url.'.sig';
        $this->errHandler->start();

        if (!$fh = fopen($this->tmpFile, 'w')) {
            $error = sprintf('Could not create file "%s": %s', $this->tmpFile, $this->errHandler->message);

        } elseif (!$this->getSignature($sigUrl, $signature)) {
            $error = sprintf($errFmt, $sigUrl, $this->errHandler->message);

        } elseif (!fwrite($fh, $this->httpClient->get($url))) {
            $error = sprintf($errFmt, $url, $this->errHandler->message);
        }

        if (is_resource($fh)) {
            fclose($fh);
        }
        $this->errHandler->stop();
        return empty($error);
    }

    /**
     * Verifies the downloaded file and saves it to the target location
     *
     * @param string $version The composer version downloaded
     * @param string $signature The digital signature to check
     * @param null|string $error Set by method on failure
     *
     * @return bool If the operation succeeded
     */
    protected function verifyAndSave($version, $signature, &$error)
    {
        $error = '';

        if (!$this->validatePhar($this->tmpFile, $pharError)) {
            $error = 'The download is corrupt: '.$pharError;

        } elseif (!$this->verifySignature($version, $signature, $this->tmpFile)) {
            $error = 'Signature mismatch, could not verify the phar file integrity';

        } else {
            $this->errHandler->start();

            if (!rename($this->tmpFile, $this->target)) {
                $error = sprintf('Could not write to file "%s": %s', $this->target, $this->errHandler->message);
            }
            chmod($this->target, 0755);
            $this->errHandler->stop();
        }

        return empty($error);
    }

    /**
     * Parses an array of version data to match the required channel
     *
     * @param array $data Downloaded version data
     * @param mixed $channel Version channel to use
     * @param false|string $version Set by method
     * @param mixed $url The versioned url, set by method
     */
    protected function parseVersionData(array $data, $channel, &$version, &$url)
    {
        foreach ($data[$channel] as $candidate) {
            if ($candidate['min-php'] <= PHP_VERSION_ID) {
                $version = $candidate['version'];
                $url = $this->baseUrl.$candidate['path'];
                break;
            }
        }

        if (!$version) {
            $error = sprintf(
                'None of the %d %s version(s) of Composer matches your PHP version (%s / ID: %d)',
                count($data[$channel]),
                $channel,
                PHP_VERSION,
                PHP_VERSION_ID
            );
            throw new RuntimeException($error);
        }
    }

    /**
     * Downloads the digital signature of required phar file
     *
     * @param string $url The signature url
     * @param null|string $signature Set by method on success
     *
     * @return bool If the download succeeded
     */
    protected function getSignature($url, &$signature)
    {
        if (!$result = $this->disableTls) {
            $signature = $this->httpClient->get($url);

            if ($signature) {
                $signature = json_decode($signature, true);
                $signature = base64_decode($signature['sha384']);
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Verifies the signature of the downloaded phar
     *
     * @param string $version The composer versione
     * @param string $signature The downloaded digital signature
     * @param string $file The temp phar file
     *
     * @return bool If the operation succeeded
     */
    protected function verifySignature($version, $signature, $file)
    {
        if (!$result = $this->disableTls) {
            $path = preg_match('{^[0-9a-f]{40}$}', $version) ? $this->pubKeys['dev'] : $this->pubKeys['tags'];
            $pubkeyid = openssl_pkey_get_public('file://'.$path);

            $result = 1 === openssl_verify(
                file_get_contents($file),
                $signature,
                $pubkeyid,
                $this->algo
            );

            openssl_free_key($pubkeyid);
        }

        return $result;
    }

    /**
     * Validates the downloaded phar file
     *
     * @param string $pharFile The temp phar file
     * @param null|string $error Set by method on failure
     *
     * @return bool If the operation succeeded
     */
    protected function validatePhar($pharFile, &$error)
    {
        if (ini_get('phar.readonly')) {
            return true;
        }

        try {
            // Test the phar validity
            $phar = new Phar($pharFile);
            // Free the variable to unlock the file
            unset($phar);
            $result = true;

        } catch (Exception $e) {
            if (!$e instanceof UnexpectedValueException && !$e instanceof PharException) {
                throw $e;
            }
            $error = $e->getMessage();
            $result = false;
        }
        return $result;
    }

    /**
     * Returns a string representation of the last json error
     *
     * @return string The error string or code
     */
    protected function getJsonError()
    {
        if (function_exists('json_last_error_msg')) {
            return json_last_error_msg();
        } else {
            return 'json_last_error = '.json_last_error();
        }
    }

    /**
     * Cleans up resources at the end of the installation
     *
     * @param bool $result If the installation succeeded
     */
    protected function cleanUp($result)
    {
        if (!$result) {
            // Output buffered errors
            if ($this->quiet) {
                $this->outputErrors();
            }
            // Clean up stuff we created
            $this->uninstall();
        }
    }

    /**
     * Outputs unique errors when in quiet mode
     *
     */
    protected function outputErrors()
    {
        $errors = explode(PHP_EOL, ob_get_clean());
        $shown = array();

        foreach ($errors as $error) {
            if ($error && !in_array($error, $shown)) {
                out($error, 'error');
                $shown[] = $error;
            }
        }
    }

    /**
     * Uninstalls newly-created files and directories on failure
     *
     */
    protected function uninstall()
    {
        foreach (array_reverse($this->installs) as $target) {
            if (is_file($target)) {
                @unlink($target);
            } elseif (is_dir($target)) {
                @rmdir($target);
            }
        }

        if (file_exists($this->tmpFile)) {
            @unlink($this->tmpFile);
        }
    }

    public static function getPKDev()
    {
        return <<<PKDEV
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAnBDHjZS6e0ZMoK3xTD7f
FNCzlXjX/Aie2dit8QXA03pSrOTbaMnxON3hUL47Lz3g1SC6YJEMVHr0zYq4elWi
i3ecFEgzLcj+pZM5X6qWu2Ozz4vWx3JYo1/a/HYdOuW9e3lwS8VtS0AVJA+U8X0A
hZnBmGpltHhO8hPKHgkJtkTUxCheTcbqn4wGHl8Z2SediDcPTLwqezWKUfrYzu1f
o/j3WFwFs6GtK4wdYtiXr+yspBZHO3y1udf8eFFGcb2V3EaLOrtfur6XQVizjOuk
8lw5zzse1Qp/klHqbDRsjSzJ6iL6F4aynBc6Euqt/8ccNAIz0rLjLhOraeyj4eNn
8iokwMKiXpcrQLTKH+RH1JCuOVxQ436bJwbSsp1VwiqftPQieN+tzqy+EiHJJmGf
TBAbWcncicCk9q2md+AmhNbvHO4PWbbz9TzC7HJb460jyWeuMEvw3gNIpEo2jYa9
pMV6cVqnSa+wOc0D7pC9a6bne0bvLcm3S+w6I5iDB3lZsb3A9UtRiSP7aGSo7D72
8tC8+cIgZcI7k9vjvOqH+d7sdOU2yPCnRY6wFh62/g8bDnUpr56nZN1G89GwM4d4
r/TU7BQQIzsZgAiqOGXvVklIgAMiV0iucgf3rNBLjjeNEwNSTTG9F0CtQ+7JLwaE
wSEuAuRm+pRqi8BRnQ/GKUcCAwEAAQ==
-----END PUBLIC KEY-----
PKDEV;
    }

    public static function getPKTags()
    {
        return <<<PKTAGS
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0Vi/2K6apCVj76nCnCl2
MQUPdK+A9eqkYBacXo2wQBYmyVlXm2/n/ZsX6pCLYPQTHyr5jXbkQzBw8SKqPdlh
vA7NpbMeNCz7wP/AobvUXM8xQuXKbMDTY2uZ4O7sM+PfGbptKPBGLe8Z8d2sUnTO
bXtX6Lrj13wkRto7st/w/Yp33RHe9SlqkiiS4MsH1jBkcIkEHsRaveZzedUaxY0M
mba0uPhGUInpPzEHwrYqBBEtWvP97t2vtfx8I5qv28kh0Y6t+jnjL1Urid2iuQZf
noCMFIOu4vksK5HxJxxrN0GOmGmwVQjOOtxkwikNiotZGPR4KsVj8NnBrLX7oGuM
nQvGciiu+KoC2r3HDBrpDeBVdOWxDzT5R4iI0KoLzFh2pKqwbY+obNPS2bj+2dgJ
rV3V5Jjry42QOCBN3c88wU1PKftOLj2ECpewY6vnE478IipiEu7EAdK8Zwj2LmTr
RKQUSa9k7ggBkYZWAeO/2Ag0ey3g2bg7eqk+sHEq5ynIXd5lhv6tC5PBdHlWipDK
tl2IxiEnejnOmAzGVivE1YGduYBjN+mjxDVy8KGBrjnz1JPgAvgdwJ2dYw4Rsc/e
TzCFWGk/HM6a4f0IzBWbJ5ot0PIi4amk07IotBXDWwqDiQTwyuGCym5EqWQ2BD95
RGv89BPD+2DLnJysngsvVaUCAwEAAQ==
-----END PUBLIC KEY-----
PKTAGS;
    }
}
