<?php /**
 * @file
 * Contains \Drupal\update_audit\Controller\DefaultController.
 */

namespace Drupal\update_audit\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Default controller for the update_audit module.
 */
class DefaultController extends ControllerBase {

  public function update_audit_list() {
    // @FIXME
// drupal_set_title() has been removed. There are now a few ways to set the title
// dynamically, depending on the situation.
// 
// 
// @see https://www.drupal.org/node/2067859
// drupal_set_title('Update Audit');


    $render = [];


    // Atach out libs.
    $render['#attached']['css'][] = [
      'type' => 'external',
      'data' => '//cdn.jsdelivr.net/simplemde/latest/simplemde.min.css',
    ];
    $render['#attached']['css'][] = [
      'type' => 'file',
      'data' => drupal_get_path('module', 'update_audit') . '/styles/update-audit.css',
    ];



    $render['#attached']['js'][] = [
      'type' => 'external',
      'data' => '//cdn.jsdelivr.net/simplemde/latest/simplemde.min.js',
      'scope' => 'header',
    ];
    $render['#attached']['js'][] = [
      'type' => 'external',
      'data' => 'https://cdnjs.cloudflare.com/ajax/libs/to-markdown/3.0.4/to-markdown.min.js',
      'scope' => 'header',
    ];

    $render['#attached']['js'][] = [
      'type' => 'file',
      'data' => drupal_get_path('module', 'update_audit') . '/scripts/update-audit.js',
      'scope' => 'footer',
    ];



    $updates = _update_audit_get_updates();


    // State if patch statuses are expected but module not install.
    if (UPDATE_AUDIT_SHOW_PATCHES && !UPDATE_AUDIT_USE_PATCH_STATUS) {
      drupal_set_message('Patch status module is not installed, please install to view patches');
    }

    // Stop update_audit checking itself for updates (updateception!).
    if (!UPDATE_AUDIT_CHECK_SELF_UPDATE && !empty($updates['update_audit'])) {
      unset($updates['update_audit']);
    }


    // Pre-process the updates array - remove old releases and separate security
    // releases.
    //$count_before_processing = count($updates);
    $processed_updates = [];
    foreach ($updates as $module => &$update) {

      if (!empty($update['releases'])) {
        // Releases returned are exhaustive - remove all released before (and
      // including) our current version, as well as dev versions if you like.
        $existing_version = $update['existing_version'];

        // If the version is like '7.x-1.0-alpha2+24-dev then splice the string on +
        // and grab that actual current version. Don't store it though as it's
        // misleading to obfuscate this funny business.
        if (strpos($existing_version, '+') !== FALSE) {
          $version_parts = explode('+', $existing_version);
          $existing_version = $version_parts[0];
        }


        $existing_version_major = $update['existing_major'];
        $existing_version_timestamp = $update['releases'][$existing_version]['date'];
        $update['releases'] = array_filter($update['releases'], function(&$release) use ($existing_version_timestamp, $existing_version_major) {
          $is_new_release = $release['date'] > $existing_version_timestamp && $release['version_major'] >= $existing_version_major;
          $is_dev_release = !empty($release['version_extra']) && $release['version_extra'] == 'dev';
          return !$is_new_release || ($is_dev_release && UPDATE_AUDIT_REMOVE_DEV) ? FALSE : TRUE;
        });
      }

    }

    // iterate twice to grab security then optionals - this code is stupid but
    // cba to make nicer atm. @TODO Make better.
    foreach ($updates as $module => &$update) {
      if (!empty($update['releases']) && !empty($update['security updates'])) {
        // Move any modules with security updates to their own special array.
        $processed_updates['Security'][$module] = $update;

      }
    }
    foreach ($updates as $module => &$update) {
      if (!empty($update['releases']) && empty($update['security updates'])) {

        // Move any modules with security updates to their own special array.
        $processed_updates['Optional'][$module] = $update;
      }
    }


    // Generate output from both sets of arrays.
    $output = '';
    foreach ($processed_updates as $type => $updates) {
      // @FIXME
// theme() has been renamed to _theme() and should NEVER be called directly.
// Calling _theme() directly can alter the expected output and potentially
// introduce security issues (see https://www.drupal.org/node/2195739). You
// should use renderable arrays instead.
// 
// 
// @see https://www.drupal.org/node/2195739
// $output .= theme('update_audit_list', ['items' => $updates, 'type' => $type]);

    }

    $render['content']['markup'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'html-content'
          ]
        ],
    ];

    $render['content']['markup']['content'] = ['#markup' => $output];

    // Markdown export areas.
    $render['markdown_region'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'markdown-region'
          ]
        ],
    ];
    $render['markdown_region']['markdown_btn'] = [
      '#markup' => '<a href="#" class="get-markdown-btn">Get markdown!</a>'
      ];
    $render['markdown_region']['markdown_editor'] = [
      '#type' => 'textarea',
      '#attributes' => [
        'id' => [
          'markdown-editor'
          ]
        ],
    ];


    if (UPDATE_AUDIT_DEBUG) {
      $render['debug']['regular']['#markup'] = 'Regular updates:<pre>' . print_r($updates, TRUE) . '</pre>';
      $render['debug']['security']['#markup'] = 'Updates that include security releases:<pre>' . print_r($updates, TRUE) . '</pre>';
    }

    return $render;
  }

  public function _scrape_release_html($url = NULL) {

    $params = \Drupal\Component\Utility\UrlHelper::filterQueryParameters();

    if (empty($url) && !empty($params['url'])) {
      $url = urldecode($params['url']);
    }
    if (!empty($url)) {
      $url = $url ? $url : urldecode($params['url']);
      $class_to_scrape = 'pane-node-body';
      $html = HtmlDomParser::file_get_html($url);
      $output = $html->find('.' . $class_to_scrape);

      if (empty($output)) {
        $output = 'No information was available for this release :(';
      }
      else {
        $output = implode($output);
      }

      print $output;
      drupal_exit();
    }
  }

}
