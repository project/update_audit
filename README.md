# Update Audit

## Installation

- Download the module
- Run `composer install` from the module dir. If for whatever reason you can't install via composer then manually download the contents of [simple html dom parser](https://github.com/sunra/php-simple-html-dom-parser/tree/master/Src/Sunra/PhpSimple/simplehtmldom_1_5) into ./vendor/sunra/php-simple-html-dom-parser
- Enable the module.

## Usage

- The module adds a new menu item 'Update Audit' under the 'Reports > Available updates' menu in the CMS admin. 
- Hover over releases to get a nifty tooltip scraped from D.O with the release info.
- Various constants defined in `update_audit.module` can be messed with the provide a more or less verbose report.

## Trouble?
- If there is no update list then most likely `update_audit.module` was not able to run drush via `shell_exec()`. In this instance SSH into your guest VM env, cd to the module dir, ensure `update-audit.sh` is executable then execute it. This will generate a file `updates-export.inc` that the module will use instead. If this is the case you'll have to rerun the bash script to regenerate the updates-export if you want a fresh check.
- Be aware that because patched modules read as, for example, 7.x-3.2+25-dev it's really hard to check with regards to updates in between this version and current - use your noggin in these situations. @TODO: Improve this discovery of versions from patched modules.