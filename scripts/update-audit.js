(function ($) {

    var simplemde = new SimpleMDE({ element: document.getElementById("markdown-editor")});

    $('.show-release-info').click(function(e) { e.preventDefault();
        $container = $(this).closest('div.update');
        $container.find('a.release-link').each(function(i, el) {

            var $notes_region = $(el).closest('li').find('.release-notes-info'),
                url = $(el).attr("href"),
                url_encoded = encodeURIComponent(url);


            if ($notes_region.hasClass('shown')) {
                $notes_region.removeClass('shown');
            } else {
                $notes_region.addClass('shown');
            };

            if (url !== '' && $notes_region.html() == '') {
                var get_params = "url=" + url_encoded;

                var request = new XMLHttpRequest();
                request.open('GET', '/admin/reports/updates/update_audit/scrape?' + get_params, true);
                request.onload = function() {
                    if (request.status >= 200 && request.status < 400) {
                        // Success!
                        var $data = $(request.responseText);
                        $data.find('a').each(function(i, el) {
                            $(el).attr('href', 'https://drupal.org/' + $(el).attr('href'));
                        })
                        $notes_region.append($data);
                    }
                };
                request.onerror = function() {
                    // There was a connection error of some sort
                };
                request.send();
            }
        });

    });

    $('.get-markdown-btn').click(function(e){ e.preventDefault();
        var $html_copy = $('#page .html-content').clone();
        // Remove the nonsense that we dont want to show to the client.
        $html_copy.find('.update .verbose').remove();
        simplemde.value(toMarkdown($html_copy.html(), {converters: [
            {
                filter: ['div'],
                replacement: function(innerHTML, node) {
                    return innerHTML;
                }
            }
        ]}));
    })

})(jQuery);