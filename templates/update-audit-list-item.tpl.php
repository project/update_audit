<?php //dsm($item);  ?>
<div class="update update-<?php print strtolower($type); ?>" data-module="<?php print $item['name']; ?>">
  <?php print $title; ?>

  <div class="summary">
    <?php print $major_version; ?>
    <?php if ($tags_summary !== '') { print 'Fixes: ' . $tags_summary . '<br />'; } ?>
    <?php print $patch_info; ?>
  </div>
  <div class="verbose">
    <?php print $releases; ?>
  </div>

</div>
